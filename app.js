//Node modules
const path = require('path');
const bodyParser = require('body-parser');

//Middleware
const express = require('express');
const app = express();

//import routes
const workerRoutes = require('./routes/worker');
const messageRoutes = require('./routes/message');

//models import
const Worker = require('./models/Worker');
const Message = require('./models/Message');
//database
const sequelize = require('./utils/database');

// view engine setup
app.set('view engine', 'ejs');

app.use(bodyParser.urlencoded( // routes 7-12
    {extended: false})
);
//static files
app.use(express.static(path.join(__dirname,'public')));
//routes import
app.use('/worker', workerRoutes);
app.use('/message', messageRoutes);

app.get('/', (req, res) => {
    res.render('index', {
        title: 'NS app',
        description: 'NS happy travels'
    });
});
//404 handeling
app.use((req, res, next) => {
    res.status(404).render('404', {
        title: 404
    });
    console.log('app.js 404');
});
//port handeling
sequelize.sync()
    .then(result=>{
        console.log(result);
        app.listen(3000);
    })
    .catch(err=>{
        console.log(err);
    });
