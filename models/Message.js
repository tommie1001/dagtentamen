const Sequelize = require('sequelize');
const sequelize = require('../utils/database');

const messages = sequelize.define('messages',{
    id:{
        type: Sequelize.BIGINT,
        autoIncrement: true,
        allowNull:false,
        primaryKey:true
    },
    title:{
        type: Sequelize.STRING,
        allowNull:false,
    },
    spoortraject:{
        type: Sequelize.TEXT,
        allowNull:false,
    },
    kenmerken:{
        type: Sequelize.TEXT,
        allowNull:false,
    },
    station:{
        type: Sequelize.TEXT,
        allowNull:false,
    },
});
module.exports = messages;