const Sequelize = require('sequelize');
const sequelize = require('../utils/database');

const workers = sequelize.define('workers',{
    id:{
        type: Sequelize.BIGINT,
        autoIncrement: true,
        allowNull:false,
        primaryKey:true
    },
    name:{
        type: Sequelize.STRING,
        allowNull:false,
    },
    email:{
        type: Sequelize.TEXT,
        allowNull:false,
    },
    pin:{
        type: Sequelize.INTEGER,
        allowNull:false,
    },
});
module.exports = workers;