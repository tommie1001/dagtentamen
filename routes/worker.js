const path = require('path');

const express = require('express');
const router = express.Router();

const workers = require('../controllers/workers');


router.get('/register',workers.register);
router.post('/postuser',workers.postUser);



module.exports = router;