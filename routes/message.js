const path = require('path');

const express = require('express');
const router = express.Router();

const messages = require('../controllers/messages');
//all messages
router.get('/forum',messages.getMessages);
//edit specific message
router.post('/editMessage',messages.editMessage);
//upload new message
router.post('/postmessage',messages.postStoreMessage);
//update message
router.get('/updateMessage',messages.updateMessage);
//delete message
router.post('/removeMessage',messages.RemoveMessage);
//add message front-end
router.get('/addmessage', (req, res, next) => {
    res.render('medewerkers/addmessage', {
        path: '/addmessage'
    });
});


module.exports = router;