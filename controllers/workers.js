const Workers = require('../models/Worker');


exports.register = (req, res, next) => {
    res.render('medewerkers/register', {
        path: '/register',
        title: 'Forum',
        description: 'Hier staan alle gevonden voorwerpen',
    });
};

exports.postUser = (req, res, next) => {
    const name = (req.body.name);
    const email = (req.body.email);
    const pincode = (req.body.pincode);

    Workers.create({
        name: name,
        email: email,
        pin: pincode
    })
        .then(result => {
            res.redirect('/message/forum');
        }).catch(err => {
        console.log(err);
    });
};