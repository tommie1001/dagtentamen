const Message = require('../models/Message');

//get all messages
exports.getMessages = (req, res, next) => {
    Message.findAll()
        .then(messages => {
            res.render('forum/index', {
                path: '/forum',
                title: 'Forum',
                description: 'Hier staan alle gevonden voorwerpen',
                messages: messages
            });
        })
        .catch(err => {
            console.log(err);
        });
};
//upload new messages
exports.postStoreMessage = (req, res, next) => {
    const title = (req.body.title);
    const spoortraject = (req.body.traject);
    const kenmerken = (req.body.kenmerken);
    const station = (req.body.station);
    Message.create({
        title: title,
        spoortraject: spoortraject,
        kenmerken: kenmerken,
        station: station
    })
        .then(result => {
            res.redirect('/message/forum');
        }).catch(err => {
        console.log(err);
    });
};
// delete messages
exports.RemoveMessage = (req, res, next) => {
    const id = (req.body.id);
    Message.findByPk(id)
        .then(message => {
            return message.destroy();

        }).then(result => {
        res.redirect('../message/forum');
    }).catch(err => {
        console.log(err);
    })
};
//update messages
exports.updateMessage = (req, res, next) => {
    const messageId = req.params.id;
    Message.findByPk(messageId)
        .then(message => {
            message.update({
                    title: (req.params.title),
                    beschrijving: (req.params.beschrijving),
                    kenmerken: (req.params.kenmerken),
                    spoortraject: (req.params.traject),
                    station: (req.params.station),
                }, {
                    fields: ['title', 'beschrijving', 'kenmerken', 'spoortraject', 'station'],

                }
            ).then(result => {
                res.redirect('../message/forum');
            })
        })
        .catch(err => {
            console.log(err);
        });
};
//edit messages
exports.editMessage = (req, res, next) => {


    Message.findOne({
        where: {id: (req.body.id)},
        attributes: ['id']
    })
        .then(messages => {
            res.render('forum/editmessage', {
                path: '/editmessage',
                title: 'Edit message',
                description: 'Change your message here',
                items: messages
            });
        })
        .catch(err => {
            console.log(err);
        });
};
